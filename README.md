# xmark-4

A incremetal static site generator, with one theme, based on pandoc.

> Should I use this

no

> But I will anyway.

Good luck. Its completely unready, and I dont know where it's going.

Requires cargo, pandoc and ninja, and probably more

## Quickstart

```
git clone https://codeberg.org/adot/xmark-4.git --recurse-submodule
cd xmark-4
virtualenv -p python3.9 venv
source venv/bin/activate      
pip install -r requirements.txt
make test
```



```
cargo r -- corpus/rustc-dev-guide/src > build.ninja
```

---
## Random notes

- https://leanprover-community.github.io/mathlib_docs/commands.html
- https://github.com/leanprover-community/doc-gen
- https://github.com/mdx-js/rust
- https://github.com/aDotInTheVoid/xmark/
- https://codeberg.org/adot/xmark
- https://www.youtube.com/watch?v=T9uZJFO54iM

## Requirements

- Python 3.9
- Rust 1.50
