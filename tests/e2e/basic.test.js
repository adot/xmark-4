const puppeteer = require("puppeteer");

function sum(a, b) {
  return a + b;
}

test("adds 1 + 2 to equal 3", () => {
  expect(sum(1, 2)).toBe(3);
});

describe("With a browser", () => {
    // See https://dev.to/jankaritech/testing-with-jest-and-puppeteer-56k
  let browser;
  beforeAll(async () => {
    browser = await puppeteer.launch();
  });
  afterAll(async () => {
    await browser.close()
  });

  test("Can get a page", async () => {
    const ctx = await browser.createIncognitoBrowserContext();
    const page = await ctx.newPage();
    await page.goto("http://0.0.0.0:8000/appendix-01-keywords.html");

    await page.close();
    await ctx.close();
  });

  test("Can get a page 2", async () => {
    const ctx = await browser.createIncognitoBrowserContext();
    const page = await ctx.newPage();
    await page.goto("http://0.0.0.0:8000/appendix-01-keywords.html");

    await page.close();
    await ctx.close();
  });

  test("Can click around", async () => {
    const ctx = await browser.createIncognitoBrowserContext();
    const page = await ctx.newPage();

    await page.goto("http://0.0.0.0:8000/appendix-01-keywords.html");


    await page.goto("http://0.0.0.0:8000/appendix-01-keywords.html");
    {
        const targetPage = page;
        const frame = targetPage.mainFrame();
        const element = await frame.waitForSelector("aria/The Rust Programming Language");
        await element.click();
    }
    {
        const targetPage = page;
        const frame = targetPage.mainFrame();
        const element = await frame.waitForSelector("aria/9.3. To panic! or Not To panic!");
        await element.click();
    }
    {
        const targetPage = page;
        const frame = targetPage.mainFrame();
        const element = await frame.waitForSelector("aria/19.4. Advanced Functions and Closures");
        await element.click();
    }
    {
        const targetPage = page;
        const frame = targetPage.mainFrame();
        const element = await frame.waitForSelector("aria/21.7. G - How Rust is Made and “Nightly Rust”");
        await element.click();
    }
    
    await page.close();
    await ctx.close();
  });
});
