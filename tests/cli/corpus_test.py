# TODO: Read https://stackoverflow.com/a/28154841

from os.path import join as j

from util import BASE_DIR, XMARK_EXE, NINJA_EXE, assert_builds_normal


def test_build_trpl(snapshot, tmp_path):
    assert_builds_normal(
        j(BASE_DIR, "corpus", "book", "src"), tmp_path, snapshot, "trpl"
    )


def test_build_mdbook(snapshot, tmp_path):
    assert_builds_normal(
        j(BASE_DIR, "corpus", "mdbook", "guide", "src"), tmp_path, snapshot, "mdbook"
    )


def test_build_dev_guide(snapshot, tmp_path):
    assert_builds_normal(
        j(BASE_DIR, "corpus", "rustc-dev-guide", "src"), tmp_path, snapshot, "dev-guide"
    )


# TODO: Make this work
# Needs something like https://codeberg.org/adot/xmark/src/branch/trunk/ninjen/ninjen.py#L210-L224

# def test_build_cxx(tmp_path):
#     assert_builds(j(BASE_DIR, "corpus", "cxx", "book", "src"), tmp_path)
