from util import BASE_DIR, XMARK_EXE, NINJA_EXE, NINJA_FLAGS
from os.path import join as j
import os
import shutil
import subprocess

DEV_GUIDE_PATH = j(BASE_DIR, "corpus", "rustc-dev-guide", "src")

# Issue 13, First time tdd
def test_build_out_of_tree(tmp_path):
    book = shutil.copytree(DEV_GUIDE_PATH, tmp_path, dirs_exist_ok=True)
    buildin = j(book, "buildin")
    os.mkdir(buildin)
    subprocess.run([XMARK_EXE, "gen", ".", "buildin"], check=True, cwd=book)
    subprocess.run([NINJA_EXE, "-C", buildin, NINJA_FLAGS], check=True)


# Issue 14
def test_rel_path_to_xmark(tmp_path):
    book = shutil.copytree(DEV_GUIDE_PATH, tmp_path, dirs_exist_ok=True)
    os.chdir(BASE_DIR)  # FIXME: Is this racey with other tests
    subprocess.run(["target/debug/xmark", "gen", book, book], check=True)
    subprocess.run([NINJA_EXE, "-C", book, NINJA_FLAGS], check=True)
