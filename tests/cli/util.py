import os
from os.path import join as j
from os.path import dirname
import shutil
import subprocess
import re


# TODO: Less of a bodge
for i in ["/usr/bin/ninja", "/usr/local/bin/ninja"]:
    if os.path.exists(i):
        NINJA_EXE = i
        break
else:
    raise EnvironmentError("Couldn't find ninja")

# The root of the repo
BASE_DIR = dirname(dirname(dirname(__file__)))
XMARK_EXE = j(BASE_DIR, "target", "debug", "xmark")
# Change to -v to actualy run ninja
# Takes a long time
NINJA_FLAGS = "-n"


def assert_builds_normal(book, tmp_dir, snapshot, name):
    book = shutil.copytree(book, tmp_dir, dirs_exist_ok=True)
    subprocess.run([XMARK_EXE, "gen", book, book], check=True)
    how = subprocess.run(
        [NINJA_EXE, "-C", book, "-nv"],
        check=True,
        capture_output=True,
        env={"NINJA_STATUS": ""},
    )
    snapshot.assert_match(process_snapshot(how.stdout, tmp_dir), f"build.txt")
    subprocess.run([NINJA_EXE, "-C", book, NINJA_FLAGS], check=True)


# TODO: Maybe snapshot the ninja files here, so this can be the same
# as the redactions in gen.rs, or do this from rust, as insta is better
def process_snapshot(x, temp_path):
    string = x.decode()
    string = string.replace(str(temp_path), "@@TEMP_PATH@@")
    string = string.replace(BASE_DIR, "@@BASE_PATH@@")
    string = re.sub(r"(\.\./)+", "@@DOT_TO_ROOT@@", string)

    lines = string.split("\n")
    lines = sorted(lines)
    string = "\n".join(lines)

    return string
