use fs_err as fs;
use std::path::PathBuf;

use clap::Clap;
use eyre::{Result, WrapErr};

use crate::ltoc;
use crate::summary::parse_summary;

#[derive(Clap)]
pub struct Cmd {
    #[clap(long)]
    summary: PathBuf,
    #[clap(long)]
    input: PathBuf,
    #[clap(long, default_value = "/")]
    base_url: String,
    #[clap(long)]
    out: PathBuf,
}

impl Cmd {
    pub fn run(self) -> Result<()> {
        let markdown = fs::read_to_string(&self.summary)
            .wrap_err_with(|| format!("Couldnt read {:?}", &self.summary))?;

        let summary = parse_summary(&markdown)
            .wrap_err_with(|| format!("Couldn't parse {:?}", &self.summary))?;

        let ltoc = ltoc::toc(&summary, &self.base_url, &self.input)?;

        fs::write(&self.out, &ltoc)?;
        Ok(())
    }
}
