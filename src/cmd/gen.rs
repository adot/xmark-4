use fs_err as fs;
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};

use clap::Clap;
use eyre::{Context, Result};

use crate::{s_flatten, summary};

#[derive(Clap)]

pub struct Cmd {
    book_path: PathBuf,
    #[clap(default_value = ".")]
    ninja_path: PathBuf,
}

const XMARK_BASE_DIR: &str = env!("CARGO_MANIFEST_DIR");

impl Cmd {
    pub fn run(self) -> Result<()> {
        let output = gen(&self.book_path, &self.ninja_path)?;

        let n_loc = self.ninja_path.join("build.ninja");

        fs::write(n_loc, output)?;

        Ok(())
    }
}

// TODO: Move somewhere
fn gen(book_path: &Path, build_ninja_dir: &Path) -> Result<Vec<u8>> {
    // TODO: Relative path?
    let book_base_dir = Path::new("$book_base");

    let smd = book_path.join("SUMMARY.md");

    let this_binary = fs::canonicalize(std::env::args().next().unwrap())?;

    let summary_txt =
        fs::read_to_string(&smd).wrap_err_with(|| format!("Couldnt read {:?}", &smd))?;

    let summary = summary::parse_summary(&summary_txt)?;

    let items = s_flatten::flatten(summary);

    let mut n = ninjen::Writer::new();

    // TODO: Keep in sync with n.variable
    let out_dir = Path::new("$out_dir");
    let sum_dir = Path::new("$summary_dir");

    // TODO: Regen ninja file.

    let xmark_base = {
        // For _reasons_ xmark_base needs to be relative.
        // See https://codeberg.org/adot/xmark-4/issues/8
        // and https://github.com/jgm/pandoc/issues/7077

        // Find how deep into the file system
        let dirs_to_here = fs::canonicalize(build_ninja_dir)?
            .components()
            .count()
            .checked_sub(1) // Remove 1 for the `RootDir` entry, panicing instead of underflow
            .unwrap();
        let mut rel_to_root = PathBuf::new();
        for _ in 0..dirs_to_here {
            rel_to_root.push("..");
        }

        // Remove the `RootDir` from XMARK_BASE_DIR, so we can use `join`
        let mut root_to_xmark = Path::new(XMARK_BASE_DIR).components();
        root_to_xmark.next();
        rel_to_root.join(root_to_xmark.as_path())
    };

    // Vars need to be introduced before they are referenced.
    // TODO: Enforce this

    n.variable("xmark", this_binary.to_str().unwrap());
    // TODO: Handle non utf8
    n.variable("xmark_src", xmark_base.to_str().unwrap());
    n.variable("page_tpl", "$xmark_src/www/page.html");
    // TODO: Allow changing
    n.variable("build_dir", "_build");
    n.variable("summary_dir", "$build_dir/summary");
    n.variable("out_dir", "$build_dir/out");

    let mut base = pathdiff::diff_paths(book_path, build_ninja_dir).unwrap();

    if base == Path::new("") {
        base = PathBuf::from(".");
    }

    // TODO: Handle non utf8
    n.variable("book_base", base.to_str().unwrap());

    n.variable("summary", "$book_base/SUMMARY.md");

    n.newline();
    n.newline();

    n.rule((
        "pandoc",
        // TODO: Figure out titles.
        "pandoc $in -o $out --toc --metadata title=\"$title\" --template $page_tpl --css /xmark.css --include-before-body $page_ltoc",
        "PANDOC $in"
    ));
    n.newline();

    n.rule((
        "ltoc",
        "$xmark internal ltoc --summary $summary --input $in --out $out",
        "LTOC   $in",
    ));
    n.newline();

    n.rule(("cp", "cp $in $out"));
    n.newline();

    n.rule(("sass", "sass $in $out"));
    n.newline();

    n.newline();

    n.build::<_, _, &str>(ninjen::Build {
        inputs: "$xmark_src/www/xmark.scss",
        rule: "sass",
        outputs: "_build/out/xmark.css",
        ..Default::default()
    });

    n.build::<_, _, &str>(ninjen::Build {
        inputs: "$xmark_src/www/xmark.js",
        rule: "cp",
        outputs: "_build/out/xmark.js",
        ..Default::default()
    });

    n.newline();
    n.newline();

    // TODO: Cleanup
    // TODO: Normalize once, not ad hoc
    for i in items {
        if let Some(l) = i.location {
            let src = book_base_dir.join(&l);
            let mut sum = sum_dir.join(&l);

            let mut out = out_dir.join(&l);
            sum.set_extension("html");
            out.set_extension("html");

            n.build(ninjen::Build {
                outputs: &sum,
                inputs: src.clone(),
                rule: "ltoc",
                implicit: vec!["$summary", "$xmark"],
                // ..Default::default()
                pool: None,
                variables: BTreeMap::new(),
            });

            n.build(ninjen::Build {
                outputs: out,
                // TODO: store rules globaly and ensure unequeness
                rule: "pandoc",
                inputs: vec![src.as_path()],
                variables: literally::bmap! {
                    "title" => i.name.clone(),
                    "page_ltoc" => st(sum.clone()),
                },
                implicit: vec![Path::new("$page_tpl"), &sum],
                ..Default::default()
            });
            n.newline()
        }
    }
    Ok(n.done())
}

// TODO: bikeshed name
// TODO: Handle errors
fn st(p: PathBuf) -> String {
    p.into_os_string().into_string().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    mod e2e {
        use super::*;

        fn test(p: &str) {
            // TODO: Cache
            let re = regex::Regex::new("xmark-[0-9a-f]{16}").unwrap();
            let re2 = regex::Regex::new(r"(\.\./)+").unwrap();

            // TODO: Make this work anywhere?
            let c = gen(Path::new(p), Path::new(".")).unwrap();
            let mut s = String::from_utf8(c).unwrap();

            // Redact the details that would change, eg the source directory,
            // and binary name (w hash)
            s = s.replace(XMARK_BASE_DIR, "@@XMARK_BASE@@");
            let s = re.replace(&s, "@@XMARK_BIN@@");
            let s = re2.replace(&s, "@@DOT_PATH_TO_ROOT@@");
            insta::assert_snapshot!(s);
        }

        #[test]
        fn trpl() {
            test("corpus/book/src")
        }

        #[test]
        fn cxx() {
            test("corpus/cxx/book/src")
        }

        #[test]
        fn mdbook() {
            test("corpus/mdbook/guide/src")
        }

        #[test]
        fn dev_guide() {
            test("corpus/rustc-dev-guide/src")
        }
    }
}
