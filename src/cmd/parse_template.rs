use fs_err as fs;
use std::path::PathBuf;

use clap::Clap;
use eyre::Result;
use telemachus::Template;

#[derive(Clap)]
/// Parse a telemachus template to bincode
pub struct Cmd {
    #[clap(long = "in")]
    template_path: PathBuf,
    #[clap(long = "out")]
    bincode_path: PathBuf,
}

impl Cmd {
    pub fn run(self) -> Result<()> {
        let html = fs::read_to_string(&self.template_path)?;
        let tpl = Template::new(&html)?;

        let bc = bincode::serialize(&tpl)?;

        fs::write(self.bincode_path, bc)?;

        Ok(())
    }
}
