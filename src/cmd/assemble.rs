use fs_err as fs;
use std::io::BufReader;
use std::path::PathBuf;

use clap::Clap;
use eyre::{Result, WrapErr};
use serde::Serialize;
use telemachus::Template;

/// Build an output page from the content html and the template
// TODO: Ltoc & Rtoc
#[derive(Clap)]
pub struct Cmd {
    /// Path to the template, as bincode
    #[clap(long)]
    tpl: PathBuf,
    /// Path to write to
    #[clap(long)]
    out: PathBuf,
    /// Path to the inner html
    #[clap(long)]
    inner: PathBuf,
    #[clap(long)]
    title: String,
}

#[derive(Serialize)]
struct PageData<'a> {
    title: &'a str,
    content: &'a str,
}

impl Cmd {
    pub fn run(self) -> Result<()> {
        let tpl: Template = bincode::deserialize_from(BufReader::new(fs::File::open(&self.tpl)?))
            .wrap_err_with(|| format!("Failed to read from {:?}", &self.tpl))?;

        // TODO: Wrap err
        let content = fs::read_to_string(self.inner)?;

        let data = PageData {
            title: &self.title,
            content: &content,
        };

        // TODO: Wrap err
        let out = tpl.render(&data)?;

        // TODO: ditto
        fs::write(self.out, out)?;

        Ok(())
    }
}
