use clap::Clap;
use eyre::Result;

// mod assemble;
// mod parse_template;
mod gen;
mod ltoc;

#[derive(Clap)]
struct Args {
    #[clap(subcommand)]
    sub: Sub,
}
#[derive(Clap)]
enum Sub {
    Gen(gen::Cmd),
    // TODO: Maybe remove these.
    Internal(InternalW),
}

#[derive(Clap)]
struct InternalW {
    #[clap(subcommand)]
    sub: Internal,
}
#[derive(Clap)]
enum Internal {
    // Assemble(assemble::Cmd),
    // ParseTemplate(parse_template::Cmd),
    Ltoc(ltoc::Cmd),
}

pub fn main() -> Result<()> {
    Args::parse().sub.run()
}

impl Sub {
    fn run(self) -> Result<()> {
        match self {
            Sub::Gen(c) => c.run(),
            Sub::Internal(i) => {
                match i.sub {
                    // Internal::Assemble(c) => c.run(),
                    // Internal::ParseTemplate(c) => c.run(),
                    Internal::Ltoc(c) => c.run(),
                }
            }
        }
    }
}
