use std::fmt::Write;
use std::path::{Component, Path, PathBuf};

use eyre::Result;

use crate::summary::{Link, Summary, SummaryItem};

// TODO: Be clever with allocs

// TODO: Unify this with RTOC

pub fn toc(summary: &Summary, base_url: &str, in_file: &Path) -> Result<String> {
    let mut s = String::new();

    writeln!(s, "<div class=\"toc bigtoc\">")?;

    if let Some(t) = &summary.title {
        writeln!(s, "<h1>{}</h1>", t)?;
    }

    writeln!(s, "<ol>")?;

    for part in &[
        &summary.prefix_chapters,
        &summary.numbered_chapters,
        &summary.suffix_chapters,
    ] {
        for item in *part {
            render_sum_item(item, &mut s, base_url, in_file)?;
        }
    }

    writeln!(s, "</ol>")?;
    writeln!(s, "</div>")?;
    Ok(s)
}

fn render_sum_item(
    item: &SummaryItem,
    s: &mut String,
    base_url: &str,
    in_file: &Path,
) -> Result<()> {
    writeln!(s, "<li>")?;
    match item {
        SummaryItem::Link(l) => {
            // TODO: Recurse
            write_link(l, s, base_url, in_file)?;
        }
        SummaryItem::Separator => {
            writeln!(s, "<hr>")?;
        }
        SummaryItem::PartTitle(t) => {
            writeln!(s, "<h2>{}</h2>", t)?;
        }
    }
    writeln!(s, "</li>")?;

    Ok(())
}

fn write_link(l: &Link, s: &mut String, base_url: &str, in_file: &Path) -> Result<()> {
    let number = l.number.as_ref().map(|x| x.to_string());
    // TODO: Write the link
    // You'll need to know the base url, so fun

    if let Some(loc) = l.location.as_ref() {
        let loc = canocical2(&loc);

        let mut href = PathBuf::from(base_url);
        href.push(&loc);
        href.set_extension("html");

        // dbg!(&in_file, &loc);

        let class = if in_file.ends_with(loc) {
            "class=\"active\""
        } else {
            ""
        };

        writeln!(s, "<a {} href=\"{}\">", class, href.to_string_lossy())?;
    }

    if let Some(n) = number {
        writeln!(s, "<b>{}</b> {}", n, l.name)?;
    } else {
        writeln!(s, "{}", l.name)?;
    }

    if l.location.is_some() {
        writeln!(s, "</a>")?;
    }

    if !l.nested_items.is_empty() {
        writeln!(s, "<ol>")?;
        for i in &l.nested_items {
            render_sum_item(i, s, base_url, in_file)?;
        }
        writeln!(s, "</ol>")?;
    }

    Ok(())
}

fn canocical2(p: &Path) -> PathBuf {
    p.components().filter(|x| x != &Component::CurDir).collect()
}

#[cfg(test)]
mod tests {
    use crate::summary::SectionNumber;

    use super::*;

    /*
    --- a/src/cmd/parse_summary.rs
    +++ b/src/cmd/parse_summary.rs
    @@ -20,6 +20,9 @@ impl Cmd {
                 .wrap_err_with(|| format!("Couldnt read {:?}", &self.markdown_in))?;
             let summary = parse_summary(&markdown)
                 .wrap_err_with(|| format!("Couldn't parse {:?}", &self.markdown_in))?;
    +
    +        println!("{:#?}", &summary);
    +
             let json = serde_json::to_string(&summary)?;
             fs::write(&self.json_out, json)
                 .wrap_err_with(|| format!("Couldnt write to {:?}", &self.json_out))?;

         cargo r -- internal parse-summary --markdown-in x.md --json-out /dev/null \
          | sed 's/ "\./  PathBuf::from("./g' | sed 's/\.md"/.md")/g' \
          | sed 's/\[/vec![/g' | sed 's/",/".to_owned(),/g' \
          | sed 's/Link(/SummaryItem::Link(/g' \
          | sed 's/PartTitle/SummaryItem::PartTitle/g' \
        | sed 's/Separator/SummaryItem::Separator/g' | pbcopy

        */
    fn asum() -> Summary {
        Summary {
            title: Some("A Book".to_owned()),
            prefix_chapters: vec![
                SummaryItem::Link(Link {
                    name: "a prefix chaper".to_owned(),
                    location: Some(PathBuf::from("./prefix.md")),
                    number: None,
                    nested_items: vec![],
                }),
                SummaryItem::Separator,
            ],
            numbered_chapters: vec![
                SummaryItem::PartTitle("Parts1".to_owned()),
                SummaryItem::Link(Link {
                    name: "a".to_owned(),
                    location: Some(PathBuf::from("./a.md")),
                    number: Some(SectionNumber(vec![1])),
                    nested_items: vec![],
                }),
                SummaryItem::Link(Link {
                    name: "b".to_owned(),
                    location: Some(PathBuf::from("./b.md")),
                    number: Some(SectionNumber(vec![2])),
                    nested_items: vec![],
                }),
                SummaryItem::Separator,
            ],
            suffix_chapters: vec![SummaryItem::Link(Link {
                name: "postfix".to_owned(),
                location: Some(PathBuf::from("./postfix.md")),
                number: None,
                nested_items: vec![],
            })],
        }
    }

    #[test]
    fn main() {
        insta::assert_snapshot!(toc(&asum(), "/book/", Path::new("foo/bar/b.md")).unwrap())
    }
}
