use std::mem;

use crate::summary::{Link, Summary, SummaryItem};

// For the LTOC, we need a Summary, but for the ninja, we
// Only need every link

pub fn flatten(s: Summary) -> Vec<Link> {
    let mut r = Vec::new();

    extend(s.prefix_chapters, &mut r);
    extend(s.numbered_chapters, &mut r);
    extend(s.suffix_chapters, &mut r);

    r
}

fn extend(items: Vec<SummaryItem>, to: &mut Vec<Link>) {
    for i in items {
        if let SummaryItem::Link(mut l) = i {
            let nexted = mem::take(&mut l.nested_items);
            to.push(l);
            extend(nexted, to);
        }
    }
}
