//! The xmark.toml File
use serde::{Deserialize, Serialize};
use toml;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
struct DiskConf {
    books: Vec<DiskLocation>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(untagged)]
enum DiskLocation {
    Bare(String),
    Named { name: String, location: String },
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Conf {
    books: Vec<Location>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
struct Location {
    name: String,
    location: String,
}

impl From<DiskLocation> for Location {
    fn from(l: DiskLocation) -> Self {
        match l {
            DiskLocation::Bare(n) => Self {
                name: n.clone(),
                location: n,
            },
            DiskLocation::Named { name, location } => Self { name, location },
        }
    }
}

impl From<DiskConf> for Conf {
    fn from(c: DiskConf) -> Self {
        Self {
            books: c.books.into_iter().map(Into::into).collect(),
        }
    }
}

impl Conf {
    pub fn from_str(r: &str) -> eyre::Result<Self> {
        let c: DiskConf = toml::from_str(r)?;
        Ok(c.into())
    }
}

#[cfg(test)]
mod tests {
    use fs_err as fs;

    use super::*;
    use insta::{assert_yaml_snapshot, glob};

    #[test]
    fn insta_e2e() {
        glob!("testdata/xmark-toml/*.toml", |p| {
            let input = fs::read_to_string(p).unwrap();
            let c = Conf::from_str(&input).unwrap();
            assert_yaml_snapshot!(c);
        })
    }
}
