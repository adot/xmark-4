mod cmd;
mod ltoc;
mod s_flatten;
mod summary;
mod xmark_toml;

fn main() -> eyre::Result<()> {
    color_eyre::install()?;

    cmd::main()
}
