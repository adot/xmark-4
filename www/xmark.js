// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

window.onload = function () {
  // Move the current page to the center of the ltoc

  var activeSection = document
    .getElementsByClassName("bigtoc")[0]
    .querySelector(".active");

  if (activeSection) {
    activeSection.scrollIntoView({ block: "center" });
  }

  // Highlight the selected items on the right
  // Loosly based on https://github.com/hakimel/css/blob/master/progress-nav/script.js

  let rtoc = document.getElementsByClassName("smalltoc")[0];
  let rtoc_items = [].slice.call(rtoc.querySelectorAll("li")).map((item) => {
    let anchor = item.querySelector("a");
    let target = document.getElementById(anchor.getAttribute("href").slice(1));

    return { anchor, target };
  });

  function undate_rtoc() {
    let window_height = window.innerHeight;

    let in_items = [];
    let out_items = [];

    rtoc_items.forEach((item) => {
      let target_bounds = item.target.getBoundingClientRect();
      if (target_bounds.bottom > 0 && target_bounds.top < window_height) {
        in_items.push(item);
      } else {
        out_items.push(item);
      }

      // If nothing would be on screan, keep what we previously had
      // TODO: Scroll the RTOC
      // http://[::]:8000/trpl/ch02-00-guessing-game-tutorial.html
      if (in_items.length != 0) {
        in_items.forEach((item) => item.anchor.classList.add("active"));
        out_items.forEach((item) => item.anchor.classList.remove("active"));
      }
    });
  }

  window.addEventListener("scroll", undate_rtoc, false);
};
