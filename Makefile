

.PHONY: fmt
fmt:
	cargo fmt
#	./node_modules/.bin/js-beautify -r www/*.html
#	See comment in www/page.html on why we cant do this
	./node_modules/.bin/prettier -w www/*.scss
	black tests/cli

.PHONY: gen
gen:
	cargo run -- gen corpus/rustc-dev-guide/src .

.PHONY: how
how:
	NINJA_STATUS= ninja -nv

.PHONY: build
build: gen
	ninja

.PHONY: serve
serve: build
	python3 -m http.server -d _build/out -b 0.0.0.0

.PHONY: insta-update
bless:
	cargo insta test --accept --delete-unreferenced-snapshots 
	 pytest tests/cli --snapshot-update --allow-snapshot-deletion || true
.PHONY: test
test:
	cargo test
# Otherwize target/debug/xmark doesn't exist
	cargo build
	pytest tests/cli

.PHONY: clean
clean:
	ninja -t clean